<?php

interface ResponseDao {
    public function getResponse(Reponse $reponse);
    public function getAll();
    public function addReponse(Reponse $reponse);
    public function updateUser(Reponse $reponse);
    public function deleteUser(Reponse $reponse);
}