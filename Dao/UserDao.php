<?php

interface UserDao {
    public function getUserById(User $user
    );
    public function getAllUser();
    public function addUser(User $user);
    public function updateUser(User $user);
    public function deleteUser(User $user);
}

