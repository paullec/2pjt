<?php


interface Champs
{
    public function getChamps($id);

    public function getAllChamps();

    public function addChamps(Champs $champs);

    public function updateChamps(Champs $champs);

    public function deleteChamps(Champs $champs);

}