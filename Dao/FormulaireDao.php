<?php


interface FormulaireDao
{
    public function getFormulaire($id);

    public function getAllFormulaire();

    public function addFormulaire(Formulaire $formulaire);

    public function deleteFormulaire();

    public function updateFormulaire();
}