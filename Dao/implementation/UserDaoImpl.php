<?php
require_once __DIR__ . '/../AbstractPdoConnector.php';
require_once __DIR__ . '/../UserDao.php';

class UserDaoImpl extends AbstractPdoConnector implements UserDao
{

    public function getUserById(User $user)
    {
        $query = "SELECT * FROM user WHERE id= :id";

        $statement = $this->pdo->prepare($query);

        $statement->execute([
            ':id' => $user->getId()
        ]);

        $statement->setFetchMode(\PDO::FETCH_CLASS, 'Reponse');

        return $statement->fetch();
    }

    public function loginUser(User $user) {
        $query = "SELECT * FROM user WHERE pseudo = :pseudo AND password = :password";

        $statement = $this->pdo->prepare($query);

        $statement->execute([
            ':pseudo' => $user->getPseudo(),
            ':password' => $user->getPassword()
        ]);

        $statement->setFetchMode(\PDO::FETCH_CLASS, 'User');
        return $statement->fetch();
    }

    public function getAllUser()
    {
        $query = "SELECT * FROM user";

        $statement = $this->pdo->prepare($query);

        $statement->execute();

        $statement->setFetchMode(\PDO::FETCH_CLASS, 'User');

        return $statement->fetchAll();
    }

    public function addUser(User $user)
    {
        $statement = $this->pdo->prepare('INSERT INTO user(pseudo, password, mail) VALUES (:pseudo, :password, :mail)');
        $statement->execute([
            ':pseudo' => $user->getPseudo(),
            ':password'  => $user->getPassword(),
            ':mail' => $user->getMail()
        ]);

        return $this->pdo->lastInsertId();
    }

    public function updateUser(User $user)
    {
        $statement = $this->pdo->prepare('UPDATE user SET pseudo = :pseudo , password = :password , mail = :mail
                                                    WHERE id = :id');
        $statement->execute([
            ':pseudo' => $user->getPseudo(),
            ':password' => $user->getPassword(),
            ':mail' => $user->getMail(),
            ':id' => $user->getId()
        ]);
    }

    public function deleteUser(User $user)
    {
        $statement = $this->pdo->prepare('DELETE FROM user WHERE id = :id');
        $statement->execute(
            [':id' => $user->getId()]
        );
    }
}

