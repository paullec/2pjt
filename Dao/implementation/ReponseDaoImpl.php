<?php

require_once __DIR__ . "/../AbstractPdoConnector.php";
require_once __DIR__ . "/../ResponseDao.php";
class ReponseDaoImpl extends AbstractPdoConnector implements ResponseDao
{
    public function getResponse(Reponse $reponse)
    {
        $query = "SELECT * FROM reponse WHERE id= :id";

        $statement = $this->pdo->prepare($query);

        $statement->execute([
            ':id' => $reponse->getId()
        ]);

        $statement->setFetchMode(\PDO::FETCH_CLASS, 'Reponse');

        return $statement->fetch();
    }

    public function getAll()
    {
        $query = "SELECT * FROM reponse";

        $statement = $this->pdo->prepare($query);

        $statement->execute();

        $statement->setFetchMode(\PDO::FETCH_CLASS, 'Reponse');

        return $statement->fetchAll();
    }

    public function addReponse(Reponse $reponse)
    {
        $statement = $this->pdo->prepare('INSERT INTO user(id_champs, value) VALUES (:id_champs, :value)');
        $statement->execute([
            ':id_champs' => $reponse->getIdChamps(),
            ':value'  => $reponse->getValue()
        ]);

        return $this->pdo->lastInsertId();
    }

    public function updateUser(Reponse $reponse)
    {
        $statement = $this->pdo->prepare('UPDATE user SET id_champs = :id_champs , value = :value WHERE id = :id');
        $statement->execute([
            ':id_champs' => $reponse->getIdChamps(),
            ':value' => $reponse->getValue()
        ]);
    }

    public function deleteUser(Reponse $reponse)
    {
        $statement = $this->pdo->prepare('DELETE FROM reponse WHERE id = :id');
        $statement->execute(
            [':id' => $reponse->getId()]
        );
    }

}