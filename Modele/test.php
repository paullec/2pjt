<?php


class test
{
    private $name;
    private $pseudo;
    private $age;
    private $something;
    private $qsdfzev;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPseudo()
    {
        return $this->pseudo;
    }

    /**
     * @param mixed $pseudo
     */
    public function setPseudo($pseudo)
    {
        $this->pseudo = $pseudo;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @return mixed
     */
    public function getSomething()
    {
        return $this->something;
    }

    /**
     * @param mixed $something
     */
    public function setSomething($something)
    {
        $this->something = $something;
    }

    /**
     * @return mixed
     */
    public function getQsdfzev()
    {
        return $this->qsdfzev;
    }

    /**
     * @param mixed $qsdfzev
     */
    public function setQsdfzev($qsdfzev)
    {
        $this->qsdfzev = $qsdfzev;
    }


}