<?php


class Reponse
{
    private $id;
    private $id_champs;
    private $value;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdChamps()
    {
        return $this->id_champs;
    }

    /**
     * @param mixed $id_champs
     */
    public function setIdChamps($id_champs)
    {
        $this->id_champs = $id_champs;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }


}